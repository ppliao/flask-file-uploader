FROM ubuntu:latest
MAINTAINER Polin

ENV LISTEN_PORT=5000
EXPOSE 5000

RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential

#FROM python:3.6
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["app.py"]
