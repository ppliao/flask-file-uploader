#!flask/bin/python

# Author: Ngo Duy Khanh
# Email: ngokhanhit@gmail.com
# Git repository: https://github.com/ngoduykhanh/flask-file-uploader
# This work based on jQuery-File-Upload which can be found at https://github.com/blueimp/jQuery-File-Upload/

import os
import PIL
from PIL import Image
import simplejson
import traceback

from flask import Flask, request, render_template, redirect, url_for, send_from_directory
from flask_bootstrap import Bootstrap
from werkzeug import secure_filename
from lib.upload_file import uploadfile
from azure.storage.blob import BlockBlobService, PublicAccess


app = Flask(__name__)
app.config['SECRET_KEY'] = '7788#$%23eefEET'
app.config['UPLOAD_FOLDER'] = 'data/'
app.config['THUMBNAIL_FOLDER'] = 'data/thumbnail/'
app.config['MAX_CONTENT_LENGTH'] = 50000000000 * 1024 * 1024

ALLOWED_EXTENSIONS = set(['*','txt', 'gif', 'png', 'jpg', 'jpeg', 'bmp', 'rar', 'zip', '7zip', 'doc', 'docx'])
IGNORED_FILES = set(['.gitignore'])
bootstrap = Bootstrap(app)

# Set blob
# Create the BlockBlockService that is used to call the Blob service for the storage account
block_blob_service = BlockBlobService(account_name='smasoftblobtest', account_key='cHYkxVBSPNzLPfaQm3eO1wUkEcVgzixr3QzfzO+4nVx0hBebAlJDiFK3iN+ObJ1TvQaCC4jMedeFpffqFswF2A==')
# Create a container called 'quickstartblobs'.
container_name ='testblob2'
block_blob_service.create_container(container_name)
# Set the permission so the blobs are public.
block_blob_service.set_container_acl(container_name, public_access=PublicAccess.Container)


def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def gen_file_name(filename):
    """
    If file was exist already, rename it and return a new name
    """

    i = 1
    while os.path.exists(os.path.join(app.config['UPLOAD_FOLDER'], filename)):
        name, extension = os.path.splitext(filename)
        filename = '%s_%s%s' % (name, str(i), extension)
        i += 1

    return filename


def create_thumbnail(image):
    try:
        base_width = 80
        img = Image.open(os.path.join(app.config['UPLOAD_FOLDER'], image))
        w_percent = (base_width / float(img.size[0]))
        h_size = int((float(img.size[1]) * float(w_percent)))
        img = img.resize((base_width, h_size), PIL.Image.ANTIALIAS)
        img.save(os.path.join(app.config['THUMBNAIL_FOLDER'], image))

        return True

    except:
        print (traceback.format_exc())
        return False


@app.route("/upload", methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        files = request.files['file']

        if files:
            #filename = secure_filename(files.filename)
            filename=files.filename
            filename = gen_file_name(filename)
            mime_type = files.content_type

            if not allowed_file(files.filename):
                result = uploadfile(name=filename, type=mime_type, size=0, not_allowed_msg="File type not allowed")

            else:
                ## save file to disk
                #uploaded_file_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
                #files.save(uploaded_file_path)

                ## create thumbnail after saving
                #if mime_type.startswith('image'):
                #    create_thumbnail(filename)
                
                ## get file size after saving
                #size = os.path.getsize(uploaded_file_path)
                block_blob_service.create_blob_from_stream(container_name, filename, files)
                size = block_blob_service.get_blob_properties(container_name,filename).properties.content_length
                #print(size)


                # return json for js call back
                result = uploadfile(name=filename, type=mime_type, size=size)

            
            return simplejson.dumps({"files": [result.get_file()]})

    if request.method == 'GET':
        # get all file in ./data directory
        files = [f for f in os.listdir(app.config['UPLOAD_FOLDER']) if os.path.isfile(os.path.join(app.config['UPLOAD_FOLDER'],f)) and f not in IGNORED_FILES ]
        
        file_display = []

        for f in files:
            size = os.path.getsize(os.path.join(app.config['UPLOAD_FOLDER'], f))
            file_saved = uploadfile(name=f, size=size)
            file_display.append(file_saved.get_file())

        return simplejson.dumps({"files": file_display})

    return redirect(url_for('index'))


@app.route("/delete/<string:filename>", methods=['DELETE'])
def delete(filename):
    # file_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
    # file_thumb_path = os.path.join(app.config['THUMBNAIL_FOLDER'], filename)

    # if os.path.exists(file_path):
    #     try:
    #         os.remove(file_path)

    #         if os.path.exists(file_thumb_path):
    #             os.remove(file_thumb_path)
            
    #         return simplejson.dumps({filename: 'True'})
    #     except:
    #         return simplejson.dumps({filename: 'False'})

    try:
        is_delete = block_blob_service.delete_blob(container_name, filename)
        #print (is_delete)
        return simplejson.dumps({filename: 'True'})
    except:
        return simplejson.dumps({filename: 'True'})


# serve static files
@app.route("/thumbnail/<string:filename>", methods=['GET'])
def get_thumbnail(filename):
    return send_from_directory(app.config['THUMBNAIL_FOLDER'], filename=filename)


@app.route("/data/<string:filename>", methods=['GET'])
def get_file(filename):
    return send_from_directory(os.path.join(app.config['UPLOAD_FOLDER']), filename=filename)


@app.route('/', methods=['GET', 'POST'])
def index():
    return render_template('index.html')


if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0')
